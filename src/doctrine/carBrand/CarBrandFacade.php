<?php

declare(strict_types=1);

namespace Skadmin\Car\Doctrine\CarBrand;

use App\Model\Doctrine\Facade;
use App\Model\Doctrine\Traits;
use Nettrine\ORM\EntityManagerDecorator;

final class CarBrandFacade extends Facade
{
    use Traits\Facade\Webalize;
    use Traits\Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = CarBrand::class;
    }

    public function create(string $name, string $content, bool $isActive, ?string $imagePreview, ?string $imageLogoPreview) : CarBrand
    {
        return $this->update(null, $name, $content, $isActive, $imagePreview, $imageLogoPreview);
    }

    public function update(?int $id, string $name, string $content, bool $isActive, ?string $imagePreview, ?string $imageLogoPreview, bool $changeWebalize = false) : CarBrand
    {
        $carBrand = $this->get($id);
        $carBrand->update($name, $content, $isActive, $imagePreview, $imageLogoPreview);

        if (! $carBrand->isLoaded()) {
            $carBrand->setWebalize($this->getValidWebalize($name));
            $carBrand->setSequence($this->getValidSequence());
        } elseif ($changeWebalize) {
            $carBrand->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($carBrand);
        $this->em->flush();

        return $carBrand;
    }

    public function get(?int $id = null) : CarBrand
    {
        if ($id === null) {
            return new CarBrand();
        }

        $carBrand = parent::get($id);

        if ($carBrand === null) {
            return new CarBrand();
        }

        return $carBrand;
    }

    /**
     * @return CarBrand[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }
}
