<?php

declare(strict_types=1);

namespace Skadmin\Car\Doctrine\CarBrand;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Skadmin\Car\Doctrine\Car\Car;
use SkadminUtils\DoctrineTraits\Entity;

/**
 * Class CarBrand
 *
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class CarBrand
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\ImagePreview;
    use Entity\IsActive;
    use Entity\Sequence;
    use Entity\Created;
    use Entity\LastUpdateDate;

    /**
     * @Doctrine\ORM\Mapping\Column(type="string", nullable=true)
     * @var string|null
     */
    private $imageLogoPreview = '';

    /**
     * @Doctrine\ORM\Mapping\OneToMany(targetEntity="\Skadmin\Car\Doctrine\Car\Car", mappedBy="carBrand")
     * @Doctrine\ORM\Mapping\OrderBy({"sequence"="ASC","name"="ASC"})
     * @var ArrayCollection|Car[]
     */
    private $cars;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
    }

    public function update(string $name, string $content, bool $isActive, ?string $imagePreview, ?string $imageLogoPreview) : void
    {
        $this->name     = $name;
        $this->content  = $content;
        $this->isActive = $isActive;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }

        if ($imageLogoPreview !== null && $imageLogoPreview !== '') {
            $this->imageLogoPreview = $imageLogoPreview;
        }
    }

    public function getImageLogoPreview() : ?string
    {
        if ($this->imageLogoPreview !== null) {
            return $this->imageLogoPreview === '' ? null : $this->imageLogoPreview;
        }
        return $this->imageLogoPreview;
    }


    /**
     * @return ArrayCollection|Car[]
     */
    public function getCars(bool $onlyActive = false)
    {
        if (! $onlyActive) {
            return $this->cars;
        }

        $criteria = Criteria::create()->where(Criteria::expr()->eq('isActive', true));
        return $this->cars->matching($criteria);
    }
}
