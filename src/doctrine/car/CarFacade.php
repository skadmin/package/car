<?php

declare(strict_types=1);

namespace Skadmin\Car\Doctrine\Car;

use App\Model\Doctrine\Facade;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Car\Doctrine\CarBrand\CarBrand;
use SkadminUtils\DoctrineTraits;

final class CarFacade extends Facade
{
    use DoctrineTraits\Facade\Sequence;
    use DoctrineTraits\Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Car::class;
    }

    public function create(string $name, string $description, string $content, bool $isActive, ?string $imagePreview) : Car
    {
        return $this->update(null, $name, $description, $content, $isActive, $imagePreview);
    }

    public function update(?int $id, string $name, string $description, string $content, bool $isActive, ?string $imagePreview) : Car
    {
        $car = $this->get($id);
        $car->update($name, $description, $content, $isActive, $imagePreview);

        if (! $car->isLoaded()) {
            $car->setWebalize($this->getValidWebalize($name));
            $car->setSequence($this->getValidSequence());
        }

        $this->em->persist($car);
        $this->em->flush();

        return $car;
    }

    public function updateCarInfo(Car $car, CarBrand $carBrand, string $cylinderVolume, ?int $fuel, ?int $transmission, int $yearOfManufacture, float $price, int $priceVat) : Car
    {
        $car->updateCarInfo($carBrand, $cylinderVolume, $fuel, $transmission, $yearOfManufacture, $price, $priceVat);
        $this->em->flush();
        return $car;
    }

    public function get(?int $id = null) : Car
    {
        if ($id === null) {
            return new Car();
        }

        $car = parent::get($id);

        if ($car === null) {
            return new Car();
        }

        return $car;
    }

    /**
     * @return Car[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];
        $orderBy  = ['sequence' => 'ASC'];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize) : ?Car
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
