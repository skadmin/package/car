<?php

declare(strict_types=1);

namespace Skadmin\Car\Doctrine\Car;

use Skadmin\Car\Doctrine\CarBrand\CarBrand;
use SkadminUtils\DoctrineTraits\Entity;
use function sprintf;

/**
 * Class Car
 *
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\HasLifecycleCallbacks
 */
class Car
{
    use Entity\Id;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Description;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\IsActive;
    use Entity\Created;
    use Entity\LastUpdateDate;

    /**
     * @Doctrine\ORM\Mapping\Column(type="text")
     * @var string
     */
    private $cylinderVolume = '';

    /**
     * @Doctrine\ORM\Mapping\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $fuel = null;

    /**
     * @Doctrine\ORM\Mapping\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $transmission = null;

    /**
     * @Doctrine\ORM\Mapping\Column(type="integer")
     * @var int
     */
    private $yearOfManufacture = 1991;

    /**
     * @Doctrine\ORM\Mapping\Column(type="float")
     * @var float
     */
    private $price = 0;

    /**
     * @Doctrine\ORM\Mapping\Column(type="integer")
     * @var int
     */
    private $priceVat = 0;

    /**
     * @Doctrine\ORM\Mapping\ManyToOne(targetEntity="\Skadmin\Car\Doctrine\CarBrand\CarBrand", inversedBy="cars")
     * @Doctrine\ORM\Mapping\JoinColumn(onDelete="cascade")
     * @var CarBrand
     */
    private $carBrand;

    public function update(string $name, string $description, string $content, bool $isActive, ?string $imagePreview) : void
    {
        $this->name        = $name;
        $this->description = $description;
        $this->content     = $content;
        $this->isActive    = $isActive;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function updateCarInfo(CarBrand $carBrand, string $cylinderVolume, ?int $fuel, ?int $transmission, int $yearOfManufacture, float $price, int $priceVat) : void
    {
        $this->carBrand          = $carBrand;
        $this->cylinderVolume    = $cylinderVolume;
        $this->fuel              = $fuel;
        $this->transmission      = $transmission;
        $this->yearOfManufacture = $yearOfManufacture;
        $this->price             = $price;
        $this->priceVat          = $priceVat;
    }

    public function getCarBrand() : CarBrand
    {
        return $this->carBrand;
    }

    public function getFullName() : string
    {
        return sprintf('%s %s', $this->getCarBrand()->getName(), $this->getName());
    }

    public function getCylinderVolume() : string
    {
        return $this->cylinderVolume;
    }

    public function getFuel() : ?int
    {
        return $this->fuel;
    }

    public function getTransmission() : ?int
    {
        return $this->transmission;
    }

    public function getYearOfManufacture() : int
    {
        return $this->yearOfManufacture;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function getPriceVat() : int
    {
        return $this->priceVat;
    }

    public function getPriceceWithVat() : float
    {
        return $this->getPrice() * (1 + $this->getPriceVat() / 100);
    }
}
