<?php

declare(strict_types=1);

namespace Skadmin\Car\Utils;

class Settings
{
    public const FUEL_DIESEL   = 1;
    public const FUEL_BENZIN   = 2;
    public const FUEL_ELECTRIC = 3;
    public const FUEL_LPG      = 4;
    public const FUEL_BIO      = 5;

    public const FUEL_TYPES = [
        self::FUEL_DIESEL   => self::FUEL_DIESEL,
        self::FUEL_BENZIN   => self::FUEL_BENZIN,
        self::FUEL_ELECTRIC => self::FUEL_ELECTRIC,
        self::FUEL_LPG      => self::FUEL_LPG,
        self::FUEL_BIO      => self::FUEL_BIO,
    ];

    public const TRANSMISSION_MANUAL    = 1;
    public const TRANSMISSION_AUTOMATIC = 2;
    public const TRANSMISSION_CVT       = 3;

    public const TRANSMISSION_TYPES = [
        self::TRANSMISSION_MANUAL    => self::TRANSMISSION_MANUAL,
        self::TRANSMISSION_AUTOMATIC => self::TRANSMISSION_AUTOMATIC,
        self::TRANSMISSION_CVT       => self::TRANSMISSION_CVT,
    ];
}
