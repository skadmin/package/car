<?php

declare(strict_types=1);

namespace Skadmin\Car;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE = 'car';

    public const DIR_IMAGE       = 'car';
    public const DIR_IMAGE_BRAND = 'car-brand';

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-car']),
            'items'   => ['overview'],
        ]);
    }
}
