<?php

declare(strict_types=1);

namespace Skadmin\Car\Components\Admin;

interface IEditBrandFactory
{
    public function create(?int $id = null) : EditBrand;
}
