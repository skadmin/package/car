<?php

declare(strict_types=1);

namespace Skadmin\Car\Components\Admin;

use App\Components\Form\FormWithUserControl;
use App\Model\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Contributte\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Car\BaseControl;
use Skadmin\Car\Doctrine\CarBrand\CarBrand;
use Skadmin\Car\Doctrine\CarBrand\CarBrandFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class EditBrand extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    private $webLoader;

    /** @var ImageStorage */
    private $imageStorage;

    /** @var CarBrandFacade */
    private $facade;

    /** @var CarBrand */
    private $carBrand;

    public function __construct(?int $id, CarBrandFacade $facade, Translator $translator, LoaderFactory $webLoader, ImageStorage $imageStorage, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->carBrand = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->carBrand->isLoaded()) {
            return new SimpleTranslation('car-brand.edit.title - %s', $this->carBrand->getName());
        }

        return 'car-brand.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        // IDENTIFIER
        $identifier     = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE_BRAND);
        $identifierLogo = UtilsFormControl::getImagePreview($values->image_logo_preview, BaseControl::DIR_IMAGE_BRAND);

        if ($this->carBrand->isLoaded()) {
            if ($identifier !== null && $this->carBrand->getImagePreview() !== null) {
                $this->imageStorage->delete($this->carBrand->getImagePreview());
            }

            if ($identifierLogo !== null && $this->carBrand->getImageLogoPreview() !== null) {
                $this->imageStorage->delete($this->carBrand->getImageLogoPreview());
            }

            $carBrand = $this->facade->update(
                $this->carBrand->getId(),
                $values->name,
                $values->content,
                $values->is_active,
                $identifier,
                $identifierLogo,
                $values->change_webalize
            );
            $this->onFlashmessage('form.car-brand.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $carBrand = $this->facade->create(
                $values->name,
                $values->content,
                $values->is_active,
                $identifier,
                $identifierLogo
            );
            $this->onFlashmessage('form.car-brand.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-brand',
            'id'      => $carBrand->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-brand',
        ]);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editBrand.latte');

        $template->carBrand = $this->carBrand;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.car-brand.edit.name')
            ->setRequired('form.car-brand.edit.name.req');
        $form->addTextArea('content', 'form.car-brand.edit.content');
        $form->addCheckbox('is_active', 'form.car-brand.edit.is-active')
            ->setDefaultValue(true);

        $form->addImageWithRFM('image_preview', 'form.car-brand.edit.image-preview');
        $form->addImageWithRFM('image_logo_preview', 'form.car-brand.edit.image-logo-preview');

        // EXTRA
        if ($this->carBrand->isLoaded()) {
            $form->addCheckbox('change_webalize', Html::el('sup', [
                'title'          => $this->translator->translate('form.car-brand.edit.change-webalize'),
                'class'          => 'far fa-fw fa-question-circle',
                'data-toggle'    => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // BUTTON
        $form->addSubmit('send', 'form.car-brand.edit.send');
        $form->addSubmit('send_back', 'form.car-brand.edit.send-back');
        $form->addSubmit('back', 'form.car-brand.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->carBrand->isLoaded()) {
            return [];
        }

        return [
            'name'     => $this->carBrand->getName(),
            'content'  => $this->carBrand->getContent(),
            'is_active' => $this->carBrand->isActive(),
        ];
    }
}
