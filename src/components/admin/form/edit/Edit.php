<?php

declare(strict_types=1);

namespace Skadmin\Car\Components\Admin;

use App\Components\Form\FormWithUserControl;
use App\Model\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Contributte\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Car\BaseControl;
use Skadmin\Car\Doctrine\Car\Car;
use Skadmin\Car\Doctrine\Car\CarFacade;
use Skadmin\Car\Doctrine\CarBrand\CarBrandFacade;
use Skadmin\Car\Utils\Settings;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function date;
use function sprintf;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var CarFacade */
    private $facade;

    /** @var CarBrandFacade */
    private $facadeCarBrand;

    /** @var Car */
    private $car;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(?int $id, CarFacade $facade, CarBrandFacade $facadeCarBrand, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade         = $facade;
        $this->facadeCarBrand = $facadeCarBrand;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->car = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->car->isLoaded()) {
            return new SimpleTranslation('car.edit.title - %s', $this->car->getFullName());
        }

        return 'car.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->car->isLoaded()) {
            if ($identifier !== null && $this->car->getImagePreview() !== null) {
                $this->imageStorage->delete($this->car->getImagePreview());
            }

            $car = $this->facade->update($this->car->getId(), $values->name, $values->description, $values->content, $values->is_active, $identifier);
            $this->onFlashmessage('form.car.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $car = $this->facade->create($values->name, $values->description, $values->content, $values->is_active, $identifier);
            $this->onFlashmessage('form.car.edit.flash.success.create', Flash::SUCCESS);
        }

        $this->facade->updateCarInfo(
            $car,
            $this->facadeCarBrand->get($values->car_brand_id),
            $values->cylinder_volume,
            $values->fuel,
            $values->transmission,
            $values->year_of_manufacture,
            $values->price,
            $values->price_vat
        );

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $car->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    public function render() : void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->car = $this->car;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // DATA
        $dataCarBrand     = $this->facadeCarBrand->getPairs('id', 'name');
        $dataFuel         = Arrays::map(Settings::FUEL_TYPES, fn(int $id) : string => sprintf('fuel_%d', $id));
        $dataTransmission = Arrays::map(Settings::TRANSMISSION_TYPES, fn(int $id) : string => sprintf('transmission_%d', $id));

        // INPUT
        $form->addText('name', 'form.car.edit.name')
            ->setRequired('form.car.edit.name.req');
        $form->addTextArea('content', 'form.car-brand.edit.content');
        $form->addTextArea('description', 'form.car-brand.edit.description');
        $form->addImageWithRFM('image_preview', 'form.car.edit.image-preview');

        // CAR
        $form->addText('cylinder_volume', 'form.car.edit.cylinder-volume');
        $form->addSelect('fuel', 'form.car.edit.fuel', $dataFuel)
            ->setPrompt(Constant::PROMTP);
        $form->addSelect('transmission', 'form.car.edit.transmission', $dataTransmission)
            ->setPrompt(Constant::PROMTP);
        $form->addInteger('year_of_manufacture', 'form.car.edit.year-of-manufacture')
            ->setDefaultValue(date('Y'));
        $form->addInteger('price', 'form.car.edit.price')
            ->setDefaultValue(0);
        $form->addInteger('price_vat', 'form.car.edit.price-vat')
            ->setDefaultValue(21);
        $form->addInteger('price_with_vat', 'form.car.edit.price-with-vat')
            ->setDefaultValue(0);

        $form->addSelect('car_brand_id', 'form.car.edit.car-brand-id', $dataCarBrand)
            ->setRequired('form.car.edit.car-brand-id.req')
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        // CHECKBOX
        $form->addCheckbox('is_active', 'form.car.edit.is-active')
            ->setDefaultValue(true);

        // EXTRA
        if ($this->car->isLoaded()) {
            $form->addCheckbox('change_webalize', Html::el('sup', [
                'title'          => $this->translator->translate('form.car-brand.edit.change-webalize'),
                'class'          => 'far fa-fw fa-question-circle',
                'data-toggle'    => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // BUTTON
        $form->addSubmit('send', 'form.car.edit.send');
        $form->addSubmit('send_back', 'form.car.edit.send-back');
        $form->addSubmit('back', 'form.car.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->car->isLoaded()) {
            return [];
        }

        return [
            'name'                => $this->car->getName(),
            'content'             => $this->car->getContent(),
            'description'         => $this->car->getDescription(),
            'cylinder_volume'     => $this->car->getCylinderVolume(),
            'fuel'                => $this->car->getFuel(),
            'transmission'        => $this->car->getTransmission(),
            'year_of_manufacture' => $this->car->getYearOfManufacture(),
            'price'               => $this->car->getPrice(),
            'price_vat'           => $this->car->getPriceVat(),
            'price_with_vat'      => $this->car->getPriceceWithVat(),
            'car_brand_id'        => $this->car->getCarBrand()->getId(),
            'is_active'           => $this->car->isActive(),
        ];
    }
}
