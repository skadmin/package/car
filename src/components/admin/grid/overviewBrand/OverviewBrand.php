<?php

declare(strict_types=1);

namespace Skadmin\Car\Components\Admin;

use App\Components\Grid\GridControl;
use App\Components\Grid\GridDoctrine;
use App\Model\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Contributte\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Car\BaseControl;
use Skadmin\Car\Doctrine\CarBrand\CarBrand;
use Skadmin\Car\Doctrine\CarBrand\CarBrandFacade;
use Skadmin\Translator\Translator;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function sprintf;

class OverviewBrand extends GridControl
{
    use APackageControl;
    use IsActive;

    /** @var CarBrandFacade */
    private $facade;

    /** @var LoaderFactory */
    private $webLoader;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(CarBrandFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewBrand.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'car.overview-brand.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.sequence', 'ASC'));

        // COLUMNS
        $grid->addColumnText('imageLogoPreview', '')
            ->setRenderer(function (CarBrand $carBrand) : ?Html {
                if ($carBrand->getImageLogoPreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$carBrand->getImageLogoPreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center')
            ->getElementPrototype('th')->addAttributes(['style' => 'width: 1px;']);
        $grid->addColumnText('name', 'grid.car.overview-brand.name')
            ->setRenderer(function (CarBrand $carBrand) : Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-brand',
                        'id'      => $carBrand->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $render = new Html();
                $name->setText($carBrand->getName());
                $render->addHtml($name);

                return $render;
            });
        $this->addColumnIsActive($grid, 'car-brand.overview');
        $grid->addColumnText('car-count', 'grid.car.overview-brand.car-count')
            ->setAlign('center')
            ->setRenderer(fn(CarBrand $cb) : int => $cb->getCars()->count());

        // FILTER
        $grid->addFilterText('name', 'grid.car.overview-brand.name');
        $this->addFilterIsActive($grid, 'car-brand.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.car.overview-brand.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-brand',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.car.overview.action.car', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('car')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.car.overview-brand.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-brand',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $item_id, ?string $prev_id, ?string $next_id) : void
    {
        $this->facade->sort($item_id, $prev_id, $next_id);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.car.overview-brand.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
