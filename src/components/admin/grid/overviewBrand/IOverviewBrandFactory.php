<?php

declare(strict_types=1);

namespace Skadmin\Car\Components\Admin;

interface IOverviewBrandFactory
{
    public function create() : OverviewBrand;
}
