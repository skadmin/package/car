<?php

declare(strict_types=1);

namespace Skadmin\Car\Components\Admin;

use App\Components\Grid\GridControl;
use App\Components\Grid\GridDoctrine;
use App\Model\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Contributte\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Car\BaseControl;
use Skadmin\Car\Doctrine\Car\Car;
use Skadmin\Car\Doctrine\Car\CarFacade;
use Skadmin\Car\Doctrine\CarBrand\CarBrandFacade;
use Skadmin\Translator\Translator;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function sprintf;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    /** @var CarFacade */
    private $facade;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var CarBrandFacade */
    private $facadeCarBrand;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(CarFacade $facade, CarBrandFacade $facadeCarBrand, Translator $translator, User $user, ImageStorage $imageStorage, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade         = $facade;
        $this->facadeCarBrand = $facadeCarBrand;
        $this->imageStorage   = $imageStorage;
        $this->webLoader      = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'car.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DATA
        $dataCarBrand = $this->facadeCarBrand->getPairs('id', 'name');

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.sequence', 'ASC')
            ->addOrderBy('a.name', 'ASC'));

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (Car $car) : ?Html {
                if ($car->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$car->getImagePreview(), '80x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center')
            ->getElementPrototype('th')->addAttributes(['style' => 'width: 1px;']);
        $grid->addColumnText('name', 'grid.car.overview.name')
            ->setRenderer(function (Car $car) : Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit',
                        'id'      => $car->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $render = new Html();
                $name->setText($car->getName());
                $render->addHtml($name);

                return $render;
            });
        $grid->addColumnText('carBrand', 'grid.car.overview.car-brand', 'carBrand.name');
        $this->addColumnIsActive($grid, 'car.overview');

        // FILTER
        $grid->addFilterText('name', 'grid.car.overview.name', ['name']);
        $grid->addFilterSelect('carBrand', 'grid.car.overview.car-brand', $dataCarBrand, 'carBrand')
            ->setPrompt(Constant::PROMTP);
        $this->addFilterIsActive($grid, 'car.overview');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.car.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#1', 'grid.car.overview.action.car-brand', [
            'package' => new BaseControl(),
            'render'  => 'overview-brand',
        ])->setIcon('list')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.car.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $item_id, ?string $prev_id, ?string $next_id) : void
    {
        $this->facade->sort($item_id, $prev_id, $next_id);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.car.overview.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
