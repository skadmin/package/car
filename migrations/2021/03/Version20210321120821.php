<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210321120821 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'car.overview', 'hash' => 'fbf51c5c022c912e5b0fcf738b635f69', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Auta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'car.overview.title', 'hash' => 'aefbb32193659af00421c4a250645a20', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Auta|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview.is-active', 'hash' => '66afcf8768b10ba7a5448a5f05867ed6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview.action.new', 'hash' => 'e8a02c65106e13b8ae5bcf8988e40f92', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit auto', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview.name', 'hash' => 'e94aa5e1227834e7d955cffb60c48882', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview.car-brand', 'hash' => '5d390b9b045592b6078a32f2c35285c9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Značka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview.action.car-brand', 'hash' => 'f044a273139d38ac54393010c14a861a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled značek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'car.overview-brand.title', 'hash' => 'a3ca6140d62dd748620b4aaae3ca2dd8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Značky aut|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview.action.car', 'hash' => 'f983b0b71c05ee7c6a8f14075fb2d9f5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled aut', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview-brand.action.new', 'hash' => 'dc27c1805be0a14f5f73cb8709956434', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit značku aut', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview-brand.name', 'hash' => '6b9411a18e87116fb6407318de930bc2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview-brand.car-count', 'hash' => 'fdaaecf3c32edf931423cc58d7767c76', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet aut', 'plural1' => '', 'plural2' => ''],
            ['original' => 'car-brand.edit.title', 'hash' => 'd0899817da138c6cb6ecdf85ffa03218', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení značky auta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'car.edit.title', 'hash' => '3520b8b88621b293733b0a68619d6c1d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení auta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.name', 'hash' => 'a3cb96af70e22c34320e84e11899293c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.name.req', 'hash' => '727d2196306da0fe3160d58c1ecc54a3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.image-preview', 'hash' => '0510894fff4ed4f46e18b4dc46f80114', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled auta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.image-preview.rule-image', 'hash' => '268670ecb26a071ff15f22a8bb9808f6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.name', 'hash' => 'ea8abdb3ae66e640944f2076f7537255', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.name.req', 'hash' => '21d136932c0b8513b8e78b36d99c1a59', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název značky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.content', 'hash' => 'c0f71e1eafce8ec32e0fbfc69933610b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.send', 'hash' => '704bb8f676e03cf3e1260aa84bfba9b7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.send-back', 'hash' => 'f0efc01ba31ab976a4f9da886cc1e822', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.back', 'hash' => 'd38b2b2ef8694f4ff6d5582d8a2aee78', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.image-preview', 'hash' => '45fa943e7ad122698db330f6197f78c3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled pozadí loga značky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.image-preview.rule-image', 'hash' => 'e5d787cb4a4f20738e59841805f95b6e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.image-logo-preview', 'hash' => '3fb405dd42173aa8fdf156eabaed57d2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled loga značky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.image-logo-preview.rule-image', 'hash' => '9562d867de0d9bdf88f3714749d1a4b4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.flash.success.create', 'hash' => 'f4a040013875739b1a883464cceafd1a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Značka auta byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.flash.success.update', 'hash' => '8260ee52ba3848c752f1f6aa65a3ebbb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Značka auta byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.flash.success.create', 'hash' => '5f571fd7b47c4b601a2028c5e680cd9b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Auto bylo úspěšně založeno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.flash.success.update', 'hash' => '2de97ec5e9a92d541aa5df2c9ed1cf0f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Auto bylo úspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.car.title', 'hash' => 'd04f8153673a6b3469b5142ec5b13402', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Auta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.car.description', 'hash' => '831d2a8a0a3a07bda3d5b30e2dcf20cd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat auta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.is-active', 'hash' => '4f7409bc30ac2b6c0486defb6ebb3fd6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car-brand.overview.is-active', 'hash' => 'a97dd5e57ce06f1e3fbf87a99b9367c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview-brand.action.edit', 'hash' => 'd13ca9b12df486543a82b7bdae66b8ef', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview-brand.action.flash.sort.success', 'hash' => '746e398ef56f8fb5e85b7ff686cced4a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí značek auto bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'car-brand.edit.title - %s', 'hash' => '65e6c479dd986371c25890f7cf5302f6', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace značky auta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.change-webalize', 'hash' => '36a864fba549f89af2bc1e952a5bef23', 'module' => 'admin', 'language_id' => 1, 'singular' => 'přegenerovat url', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview.action.edit', 'hash' => '9779091b187e1649873ab9fe8c9796c5', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.car.overview.action.flash.sort.success', 'hash' => '0de286a1733956e6c9ae2f5f024dfffa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí aut bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.car-brand-id', 'hash' => 'c4b6355c75689c07dfaf65e9c9155427', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Značka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.car-brand-id.req', 'hash' => 'eb785a58015540aea941334b4063fcfd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte prosím značku auta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.cylinder-volume', 'hash' => '65941f0d43b83b4016dc663e57db3670', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Objem motoru', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.fuel', 'hash' => '6ce3e3a830017981d89495ec86c4149b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ paliva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'fuel_1', 'hash' => 'a245f2ccd22d0e5cf3f1766f7b8da0c8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nafta', 'plural1' => '', 'plural2' => ''],
            ['original' => 'fuel_2', 'hash' => 'a7a4e9fd4fc2b311b7fdb8ac43230235', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Benzín', 'plural1' => '', 'plural2' => ''],
            ['original' => 'fuel_3', 'hash' => 'e4c74c89c9712b97e63a9db5e7a66501', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Elektro', 'plural1' => '', 'plural2' => ''],
            ['original' => 'fuel_4', 'hash' => '82539a2dfd226b2897e8c73074d148f6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'LPG', 'plural1' => '', 'plural2' => ''],
            ['original' => 'fuel_5', 'hash' => '0da2ac13b74956906bbd67dd9088fb0d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'BIO', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.transmission', 'hash' => 'b4cf31d465b07dc1bf2517fbb8fea973', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Převodovka', 'plural1' => '', 'plural2' => ''],
            ['original' => 'transmission_1', 'hash' => 'baa8085275dad478024b98a6403d28e7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Manuální', 'plural1' => '', 'plural2' => ''],
            ['original' => 'transmission_2', 'hash' => '81ec46206977d50b2018a3bfc4631731', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Automatická', 'plural1' => '', 'plural2' => ''],
            ['original' => 'transmission_3', 'hash' => 'b064793cd0b7e9121e36888471a0374d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CVT', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.year-of-manufacture', 'hash' => '96fcdcaacbd9abf495bbc1452454e3aa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Rok výroby', 'plural1' => '', 'plural2' => ''],
            ['original' => 'Please enter a valid integer.', 'hash' => 'f31ce8cf63b0480934d7e210cc7f71b6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím platné číslo', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.price', 'hash' => 'fc212a696e1e8e8bd749453e40e88dbb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Cena bez DPH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.price-vat', 'hash' => '8f316088c4f0b68ebdae66914d204205', 'module' => 'admin', 'language_id' => 1, 'singular' => 'DPH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.price-with-vat', 'hash' => '8f1c958286c2723e491c0e2867e55941', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Cena s DPH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car-brand.edit.description', 'hash' => 'cf89bba89778cc5f30a47b3bd8571284', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Krátký popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.is-active', 'hash' => 'b3bf5b7ad98ae3a97a2bb0846235194a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.send', 'hash' => 'e1a1e4cde30b05f01bb5a374c9c60fc9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.send-back', 'hash' => '8ab13bb6eac085d844dfd80642e3b190', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.car.edit.back', 'hash' => '603be6bdb37186eed9bf2a0e33692771', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'car.edit.title - %s', 'hash' => '20e1ab76795cbdedb00158beecbdd4c4', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace auta', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
